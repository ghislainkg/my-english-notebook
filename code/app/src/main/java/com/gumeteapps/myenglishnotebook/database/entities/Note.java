package com.gumeteapps.myenglishnotebook.database.entities;

import androidx.annotation.Nullable;

import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

@DatabaseTable(tableName = "Notes")
public class Note implements Persistable {

    @DatabaseField(columnName = "id", generatedId = true, canBeNull = false, unique = true)
    private int id;
    @DatabaseField(columnName = "expression", defaultValue = "")
    private String expression;
    @DatabaseField(columnName = "commentaire", defaultValue = "")
    private String commentaire;
    @DatabaseField(columnName = "category", foreign = true, foreignColumnName = "id")
    private Category category;

    private List<Traduction> traductions;
    private List<Note> synonimes;

public Note() {
    }

    public Note(String expression, String commentaire, Category category) {
        this.expression = expression;
        this.commentaire = commentaire;
        this.category = category;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return expression;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Traduction> getTraductions() {
        return traductions;
    }

    public void setTraductions(List<Traduction> traductions) {
        this.traductions = traductions;
    }

    public List<Note> getSynonimes() {
        return synonimes;
    }

    public void setSynonimes(List<Note> synonimes) {
        this.synonimes = synonimes;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", expression='" + expression + '\'' +
                ", commentaire='" + commentaire + '\'' +
                ", category=" + category +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj != null && obj.getClass() == Note.class) {
            Note n = (Note) obj;
            return this.id == n.id;
        }
        return super.equals(obj);
    }
}
