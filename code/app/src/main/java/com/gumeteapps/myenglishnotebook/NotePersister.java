package com.gumeteapps.myenglishnotebook;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.gumeteapps.myenglishnotebook.database.DataBase;
import com.gumeteapps.myenglishnotebook.database.entities.AssosSynonime;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;

import java.util.ArrayList;
import java.util.List;

public class NotePersister {

    private DataBase dataBase;

    public NotePersister(Context context) {
        dataBase = new DataBase(context);
    }

    public void persist(Note note) throws SQLiteException {
        if(note == null)
            return;
        dataBase.create(note);

        List<Traduction> traductions = note.getTraductions();
        List<Note> synonimes = note.getSynonimes();
        if(traductions == null || synonimes == null)
            return;
        for (Traduction traduction:
             traductions) {
            traduction.setNote(note);
            dataBase.create(traduction);
        }
        for (Note synonime:
                synonimes) {
            AssosSynonime assos = new AssosSynonime(note, synonime);
            dataBase.update(synonime);
            dataBase.create(assos);
        }
    }

    public void update(Note note) throws SQLiteException {
        if(note == null)
            return;
        dataBase.update(note);

        List<Traduction> traductions = note.getTraductions();
        List<Note> synonimes = note.getSynonimes();
        if(traductions == null || synonimes == null) {
            return;
        }
        for (Traduction traduction:
                traductions) {
            traduction.setNote(note);
            dataBase.update(traduction);
        }
        for (Note synonime:
                synonimes) {
            dataBase.addSynonimeToNote(note, synonime);
        }
    }

    public Note getNote(int id) {
        Note note = dataBase.getNoteById(id);
        List<Traduction> traductions = dataBase.getTraductionsByNote(note);

        List<AssosSynonime> assosSynonimes = dataBase.getAssosSynonime(note);
        List<Note> synonimes = new ArrayList<>();
        for (AssosSynonime assos :
                assosSynonimes) {
            synonimes.add(assos.getSynonime());
        }
        note.setSynonimes(synonimes);
        note.setTraductions(traductions);

        return note;
    }

    public List<Note> getAll() {
        return dataBase.getAllNotes();
    }

    public void deleteNote(Note note) {
        List<Traduction> traductions = note.getTraductions();
        if(traductions != null) {
            for (Traduction traduction :
                    traductions) {
                dataBase.delete(traduction);
            }
        }

        dataBase.delete(note);
    }
}
