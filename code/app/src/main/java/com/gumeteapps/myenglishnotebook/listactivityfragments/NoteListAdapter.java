package com.gumeteapps.myenglishnotebook.listactivityfragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.entities.Note;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteListAdapterViewHolder> {
    private SortedList<Note> sortedNotes;
    private Listener listener;

    public NoteListAdapter() {
        this.sortedNotes = new SortedList<Note>(Note.class, sortedListCallBack);
    }
    public NoteListAdapter(List<Note> notes) {
        this.sortedNotes = new SortedList<Note>(Note.class, sortedListCallBack);
        this.sortedNotes.replaceAll(notes);
    }
    public NoteListAdapter(List<Note> notes, Listener listener) {
        this(notes);
        this.listener = listener;
    }
    public NoteListAdapter(Listener listener) {
        this();
        this.listener = listener;
    }
    private SortedList.Callback<Note> sortedListCallBack = new SortedList.Callback<Note>() {
        @Override
        public int compare(Note o1, Note o2) {
            return o1.getExpression().compareTo(o2.getExpression());
        }
        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }
        @Override
        public boolean areContentsTheSame(Note oldItem, Note newItem) {
            return oldItem.getExpression().equals(newItem.getExpression());
        }
        @Override
        public boolean areItemsTheSame(Note item1, Note item2) {
            return item1.getId() == item2.getId();
        }
        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }
        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }
        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }
    };

    public void setSortedNotes(List<Note> notes) {
        this.sortedNotes.replaceAll(notes);
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public NoteListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.note_list_view_holder, parent, false);
        return new NoteListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteListAdapterViewHolder holder, int position) {
        holder.display(this.sortedNotes.get(position));
    }

    @Override
    public int getItemCount() {
        return sortedNotes.size();
    }

    class NoteListAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView expression;
        private TextView category;
        private Note note;
        NoteListAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            expression = itemView.findViewById(R.id.textViewExpressionNoteViewHolder);
            category = itemView.findViewById(R.id.textViewCategoryTitreNoteViewHolder);
            Button delete = itemView.findViewById(R.id.buttonDeleteNoteViewHolder);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        listener.onDelete(note);
                    }
                }
            });
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(note);
                    }
                }
            };
            expression.setOnClickListener(clickListener);
            category.setOnClickListener(clickListener);
        }

        void display(Note note) {
            this.note = note;
            expression.setText(note.getExpression());
            category.setText(note.getCategory().getTitre());
        }
    }

    public interface Listener {
        void onClick(Note note);
        void onDelete(Note note);
    }
}
