package com.gumeteapps.myenglishnotebook.listactivityfragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.gumeteapps.myenglishnotebook.CategoryPersister;
import com.gumeteapps.myenglishnotebook.NoteEditorActivity;
import com.gumeteapps.myenglishnotebook.NotePersister;
import com.gumeteapps.myenglishnotebook.NoteViewActivity;
import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.noteeditorfragments.NoteDialogChooseCategorie;
import com.gumeteapps.myenglishnotebook.othercomponents.DialogEditText;
import com.gumeteapps.myenglishnotebook.othercomponents.SnackBarPendingOperation;

import java.util.ArrayList;
import java.util.List;

public class NoteListFragment extends Fragment {
    private NoteListAdapter adapter;
    private NotePersister persister;

    private ListActivityViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_list_fragment, container, false);

        NoteListAdapter.Listener adapterListener = getAdapterListener(view);
        adapter = new NoteListAdapter(adapterListener);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewNoteList);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setAdapter(adapter);

        persister = new NotePersister(requireActivity());

        AppCompatImageButton addNote = view.findViewById(R.id.floatingActionButtonNoteList);
        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(requireActivity(), NoteEditorActivity.class);
                startActivity(intent);
            }
        });

        Toolbar toolbar = view.findViewById(R.id.toolbarListActivityNote);
        toolbar.setOnMenuItemClickListener(toolBarMenuListener);
        return view;
    }

    private Toolbar.OnMenuItemClickListener toolBarMenuListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch(item.getItemId()) {
                case R.id.menu_note_list_filter_option_category:
                    onFilterByCategory();
                    break;
                case R.id.menu_note_list_filter_option_contenu:
                    onFilterByContenu();
                    break;
                default:
                    return false;
            }
            return false;
        }
    };
    private void onFilterByContenu() {
        DialogEditText dialog = new DialogEditText(
                getString(R.string.dialog_filter_by_contenu_label),
                getString(R.string.btn_no_filter));
        dialog.setListener(new DialogEditText.Listener() {
            @Override
            public void onOk(String text) {
                viewModel.setContenuFilterData(text);
            }
        });
        dialog.setListenerOther(new DialogEditText.ListenerOther() {
            @Override
            public void onOther() {
                viewModel.setContenuFilterData(null);
            }
        });
        dialog.show(getParentFragmentManager(), "edit-contenu-filter");
    }
    private void onFilterByCategory() {
        CategoryPersister categoryPersister = new CategoryPersister(requireActivity());
        NoteDialogChooseCategorie dialog = NoteDialogChooseCategorie.newInstance(categoryPersister.getAll());
        dialog.setOnChooseCategoryListener(new NoteDialogChooseCategorie.OnChooseCategoryListener() {
            @Override
            public void onChoose(Category category) {
                viewModel.setCategoryFilter(category);
            }
            @Override
            public void onCancel() { }
        });
        dialog.setOtherButton(getString(R.string.btn_no_filter), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                viewModel.setCategoryFilter(null);
            }
        });
        dialog.show(getParentFragmentManager(), "choose-category-filter");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = ViewModelProviders.of(requireActivity()).get(ListActivityViewModel.class);
        // Ecoutons les notes
        viewModel.getNotesData().observe(requireActivity(), new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                adapter.setSortedNotes(notes);
            }
        });
        // Ecoutons les filtres
        viewModel.getCategoryFilterData().observe(requireActivity(), new Observer<Category>() {
            @Override
            public void onChanged(Category category) {
                // Si on change la category de filtre, on met a jour la list
                if(category == null) {
                    adapter.setSortedNotes(viewModel.getNotes());
                }
                else {
                    adapter.setSortedNotes(filterNotesByCategory(
                            viewModel.getNotes(),
                            category
                    ));
                }
            }
        });
        viewModel.getContenuFilterData().observe(requireActivity(), new Observer<String>() {
            @Override
            public void onChanged(String contenuFilter) {
                if(contenuFilter == null) {
                    adapter.setSortedNotes(viewModel.getNotes());
                }
                else {
                    List<Note> notes = filsterNotesByContenu(
                            viewModel.getNotes(),
                            contenuFilter
                    );
                    adapter.setSortedNotes(notes);
                }
            }
        });
        List<Note> notes = (new NotePersister(requireActivity())).getAll();
        adapter.setSortedNotes(notes);
    }
    private List<Note> filterNotesByCategory(List<Note> notes, Category category) {
        List<Note> res = new ArrayList<>();
        for (Note note : notes) {
            if (note.getCategory().equals(category)) {
                res.add(note);
            }
        }
        return res;
    }
    private List<Note> filsterNotesByContenu(List<Note> notes, String contenu) {
        List<Note> res = new ArrayList<>();
        for (Note note : notes) {
            if(note.getExpression().contains(contenu) || note.getCommentaire().contains(contenu)){
                res.add(note);
            }
        }
        return res;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.setNotes(persister.getAll());
    }

    private NoteListAdapter.Listener getAdapterListener(final View view) {
        return new NoteListAdapter.Listener() {
            @Override
            public void onClick(Note note) {
                Intent intent = new Intent(requireActivity(), NoteViewActivity.class);
                intent.putExtra(NoteViewActivity.EXTRA_NOTE, note.getId());
                startActivity(intent);
            }

            @Override
            public void onDelete(final Note note) {
                SnackBarPendingOperation.snack(
                        view.findViewById(R.id.layoutNoteList),
                        getString(R.string.msg_pedding_note_Delete),
                        new SnackBarPendingOperation.OnDismissListener() {
                            @Override
                            public void onAccepted() {
                                persister.deleteNote(note);
                                viewModel.removeNote(note);
                            }
                        });
            }
        };
    }
}
