package com.gumeteapps.myenglishnotebook;

import android.content.Context;

import com.gumeteapps.myenglishnotebook.database.DataBase;
import com.gumeteapps.myenglishnotebook.database.entities.Category;

import java.util.List;

public class CategoryPersister {

    private DataBase dataBase;

    public CategoryPersister(Context context) {
        dataBase = new DataBase(context);
    }

    public void persist(Category category) {
        if(category != null) {
            dataBase.create(category);
        }
    }

    public void update(Category category) {
        if(category != null) {
            dataBase.update(category);
        }
    }

    public List<Category> getAll() {
        return dataBase.getAllCategories();
    }

    public void deleteCategory(Category category) {
        dataBase.delete(category);
    }

    public Category getGlobal() {
        return dataBase.getGlobalCategory();
    }
}
