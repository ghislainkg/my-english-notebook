package com.gumeteapps.myenglishnotebook.database;

public interface Persistable {
    public int getId();
    public String getTitle();
}
