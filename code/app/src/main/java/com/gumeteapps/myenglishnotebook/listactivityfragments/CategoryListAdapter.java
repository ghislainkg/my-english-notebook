package com.gumeteapps.myenglishnotebook.listactivityfragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.entities.Category;

import java.util.ArrayList;
import java.util.List;

/*Adapter pour les listes de categories*/
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryListViewHolder> {

    private List<Category> categories;
    private Listener listener;

    public CategoryListAdapter() {
        this.categories = new ArrayList<>();
    }
    public CategoryListAdapter(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.category_list_view_holder, parent, false);
        return new CategoryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListViewHolder holder, int position) {
        holder.display(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoryListViewHolder extends RecyclerView.ViewHolder {

        private TextView titre;
        private TextView description;

        private Category category;

        CategoryListViewHolder(@NonNull View itemView) {
            super(itemView);
            titre = itemView.findViewById(R.id.textViewTitreCategoryViewHolder);
            description = itemView.findViewById(R.id.textViewDescriptionCategoryViewHolder);
            Button delete = itemView.findViewById(R.id.buttonDeleteCategoryViewHolder);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        listener.onDelete(category);
                    }
                }
            });
            titre.setOnClickListener(clickListener);
            description.setOnClickListener(clickListener);
        }

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null ){
                    listener.onClick(category);
                }
            }
        };

        void display(Category category) {
            this.category = category;
            this.titre.setText(category.getTitre());
            this.description.setText(category.getDescription());
        }
    }

    public interface Listener {
        void onClick(Category category);
        void onDelete(Category category);
    }
}
