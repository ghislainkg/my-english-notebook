package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.othercomponents.AdapterMiniPersistableList;

import java.util.ArrayList;
import java.util.List;

/*Une boite de dialogue pour choisir une categorie*/
public class NoteDialogChooseCategorie extends DialogFragment {

    /*contient les fonctions appellees quand le choix est fait ou annuler*/
    private OnChooseCategoryListener onChooseCategoryListener;
    /*Les categories a afficher*/
    private List<Category> categories;
    /*Le label d'un autre bouton*/
    private String otherButtonLabel;
    private DialogInterface.OnClickListener otherButtonListener;

    public NoteDialogChooseCategorie() { }
    public static NoteDialogChooseCategorie newInstance(List<Category> categories) {
        NoteDialogChooseCategorie res = new NoteDialogChooseCategorie();
        res.categories = categories;
        return res;
    }

    public void setOtherButton(String otherButtonLabel, DialogInterface.OnClickListener otherButtonListener) {
        this.otherButtonListener = otherButtonListener;
        this.otherButtonLabel = otherButtonLabel;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Pour creer le dialog
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.note_dialog_choose_categorie_fragment, null);

        initRecyclerView(view);

        /*On fixe la vue appropriees au builder de dialog et on lui ajoute un bouton d'annulation*/
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(onChooseCategoryListener != null) {
                            onChooseCategoryListener.onCancel();
                        }
                    }
                });
        if(this.otherButtonListener != null && this.otherButtonLabel != null) {
            builder.setNeutralButton(this.otherButtonLabel, this.otherButtonListener);
        }

        return builder.create();
    }

    /*Fonction pour changer le listener contenant les fonctions a appeller quand le
    * choix de categorie est fait on annule*/
    public void setOnChooseCategoryListener(OnChooseCategoryListener onChooseCategoryListener) {
        this.onChooseCategoryListener = onChooseCategoryListener;
    }

    private void initRecyclerView(View view) {
        //Pour initialiser le recyclerView,
        // On cree un adapter et on ecoute le click sur les items
        AdapterMiniPersistableList adapter = new AdapterMiniPersistableList(new ArrayList<Persistable>(this.categories), adapterListener);
        // On recuperer le recyclerView et lui ajoute l'adapter
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewDialogChooseCategory);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
    private AdapterMiniPersistableList.MiniPersistableListListener adapterListener =
            new AdapterMiniPersistableList.MiniPersistableListListener() {
                // L'ecoute du click sur une categorie
                @Override
                public void onClick(Persistable p) {
                    onChooseCategoryListener.onChoose((Category) p);
                    dismiss();
                }
            };

    /*Les fonctions d'ecoute ...*/
    public interface OnChooseCategoryListener{
        void onChoose(Category category);
        void onCancel();
    }
}
