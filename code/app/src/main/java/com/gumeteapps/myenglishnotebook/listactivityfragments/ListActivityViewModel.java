package com.gumeteapps.myenglishnotebook.listactivityfragments;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;

import java.util.ArrayList;
import java.util.List;

public class ListActivityViewModel extends ViewModel {

    private MutableLiveData<List<Category>> categoriesData;
    private MutableLiveData<List<Note>> notesData;
    private MutableLiveData<Category> categoryFilterData;
    private MutableLiveData<String> contenuFilterData;

    public MutableLiveData<String> getContenuFilterData() {
        if(this.contenuFilterData == null){
            this.contenuFilterData = new MutableLiveData<>();
        }
        return contenuFilterData;
    }

    public void setContenuFilterData(String contenuFilter) {
        if(this.contenuFilterData == null) {
            this.contenuFilterData = new MutableLiveData<>();
        }
        this.contenuFilterData.setValue(contenuFilter);
    }

    public MutableLiveData<Category> getCategoryFilterData() {
        if(this.categoryFilterData == null) {
            this.categoryFilterData = new MutableLiveData<>();
        }
        return this.categoryFilterData;
    }

    public void setCategoryFilter(Category categoryFilter) {
        if(this.categoryFilterData == null) {
            this.categoryFilterData = new MutableLiveData<>();
        }
        this.categoryFilterData.setValue(categoryFilter);
    }

    public MutableLiveData<List<Category>> getCategoriesData() {
        if(categoriesData == null)
            categoriesData = new MutableLiveData<>();
        return categoriesData;
    }

    public MutableLiveData<List<Note>> getNotesData() {
        if(notesData == null)
            notesData = new MutableLiveData<>();
        return notesData;
    }

    public void setCategories(List<Category> categories) {
        if(categoriesData == null) {
            categoriesData = new MutableLiveData<>();
        }
        categoriesData.setValue(categories);
    }
    public void setNotes(List<Note> notes) {
        if(notesData == null) {
            notesData = new MutableLiveData<>();
        }
        notesData.setValue(notes);
    }

    public void addCategory(Category category) {
        if(categoriesData == null) {
            categoriesData = new MutableLiveData<>();
            categoriesData.setValue(new ArrayList<Category>());
        }
        List<Category> categories = categoriesData.getValue();
        if(categories == null)
            return;
        categories.add(category);
        categoriesData.setValue(categories);
    }

    public void addNote(Note note) {
        if(notesData == null) {
            notesData = new MutableLiveData<>();
            notesData.setValue(new ArrayList<Note>());
        }
        List<Note> notes = notesData.getValue();
        if(notes == null)
            return;
        notes.add(note);
    }

    public List<Note> getNotes() {
        if(notesData == null) {
            notesData = new MutableLiveData<>();
            notesData.setValue(new ArrayList<Note>());
        }
        return notesData.getValue();
    }

    public List<Category> getCategories() {
        if(categoriesData == null) {
            categoriesData = new MutableLiveData<>();
            categoriesData.setValue(new ArrayList<Category>());
        }
        return categoriesData.getValue();
    }

    public void removeCategory(Category category) {
        if(categoriesData == null)
            return;
        List<Category> categories = categoriesData.getValue();
        if(categories == null)
            return;
        categories.remove(category);
        categoriesData.setValue(categories);
    }

    public void removeNote(Note note) {
        if(notesData == null)
            return;
        List<Note> notes = notesData.getValue();
        if(notes == null)
            return;
        notes.remove(note);
        notesData.setValue(notes);
    }

}
