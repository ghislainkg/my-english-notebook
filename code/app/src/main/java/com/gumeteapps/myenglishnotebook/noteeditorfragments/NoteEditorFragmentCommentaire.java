package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gumeteapps.myenglishnotebook.NoteViewModel;
import com.gumeteapps.myenglishnotebook.NotePersister;
import com.gumeteapps.myenglishnotebook.R;

import java.util.Objects;

/*Fragment d'ajout de commentaire*/
public class NoteEditorFragmentCommentaire extends Fragment {

    // La vue de saisie de texte
    private TextInputEditText inputEditText;
    // Le viewModel contenant les donnees globales de l'activite parente
    private NoteViewModel viewModel;
    // La navigation
    private NavController navController;

    public NoteEditorFragmentCommentaire() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_editor_commentaire_fragment, container, false);
        // On recupere toues les vues
        Button continuer = view.findViewById(R.id.buttonContinueNoteCommentaire);
        Button terminer = view.findViewById(R.id.buttonTerminerNoteCommentaire);
        Button abandonner = view.findViewById(R.id.buttonAbandonnerNoteCommentaire);
        inputEditText = view.findViewById(R.id.textInputEditNoteCommentaire);
        // On ajoute les listener a chaque bouton
        continuer.setOnClickListener(OnContinue);
        terminer.setOnClickListener(OnTerminer);
        abandonner.setOnClickListener(OnAbandonner);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // On recupere le viewModel de l'acitite parente et le navigation controller
        viewModel = ViewModelProviders.of(requireActivity()).get(NoteViewModel.class);
        navController = Navigation.findNavController(view);
    }
    @Override
    public void onResume() {
        super.onResume();
        // On prerempli avec le commentaire deja dans le viewModel
        // L'activite parente s'occupe d'ajouter ce commentaire (la note en general)
        // en fonction des extra recus
        inputEditText.setText(viewModel.getNote().getCommentaire());
    }

    private View.OnClickListener OnContinue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut passer a l'etatpe suivante de l'edition de note.
            // On met a jour le view model en y rajoutant le commentaire s'il n'est pas vide
            String text = Objects.requireNonNull(inputEditText.getText()).toString();
            if(text.isEmpty())
                Toast.makeText(requireActivity(), getString(R.string.msg_no_comment), Toast.LENGTH_SHORT).show();
            else
                viewModel.setNoteCommentaire(text);
            // Puis on vas a l'ajout de traduction
            navController.navigate(R.id.action_noteEditorFragmentCommentaire_to_noteEditorFragmentTraduction);
        }
    };

    private View.OnClickListener OnTerminer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur a fini son edition de note
            // On enregistre tout dans la base de donnees
            NotePersister persister = new NotePersister(requireActivity());
            persister.persist(viewModel.getNote());
            requireActivity().finish();
        }
    };

    private View.OnClickListener OnAbandonner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur abandonne, la note en cours d'edition ne sera pas enregistree.
            // On ferme l'activite
            requireActivity().finish();
        }
    };
}
