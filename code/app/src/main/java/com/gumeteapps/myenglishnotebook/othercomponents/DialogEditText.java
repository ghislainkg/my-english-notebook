package com.gumeteapps.myenglishnotebook.othercomponents;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.gumeteapps.myenglishnotebook.R;

import java.util.Objects;

public class DialogEditText extends DialogFragment {

    private Listener listener;
    private ListenerOther listenerOther;
    private TextInputEditText text;
    private String label;
    private String otherButtonLabel;

    public DialogEditText(String label) {
        this.label = label;
    }

    public DialogEditText(String label, String otherButtonLabel) {
        this(label);
        this.otherButtonLabel = otherButtonLabel;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setListenerOther(ListenerOther listenerOther) {
        this.listenerOther = listenerOther;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_edit_text, null, false);
        text = view.findViewById(R.id.textInputNoteViewDialogEditTraduction);
        text.setHint(label);

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setView(view)
                .setPositiveButton(R.string._str_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(listener != null) {
                            listener.onOk(Objects.requireNonNull(text.getText()).toString());
                        }
                    }
                });
        if(otherButtonLabel != null) {
            builder.setNeutralButton(otherButtonLabel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(listenerOther != null) {
                        listenerOther.onOther();
                    }
                }
            });
        }
        return builder.create();
    }

    public interface Listener {
        void onOk(String text);
    }

    public interface ListenerOther {
        void onOther();
    }
}
