package com.gumeteapps.myenglishnotebook;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;

import java.util.ArrayList;
import java.util.List;

public class NoteViewModel extends ViewModel {
    private MutableLiveData<Note> note;
    private MutableLiveData<List<Note>> synonimes;
    private MutableLiveData<List<Traduction>> traductions;
    private MutableLiveData<Boolean> close;

    private ChangeListener listener;

    public NoteViewModel() {
        close = new MutableLiveData<>();
        close.setValue(false);
    }

    public void listen(LifecycleOwner owner, ChangeListener listener) {
        this.listener = listener;
        note.observe(owner, new Observer<Note>() {
            @Override
            public void onChanged(Note note) {
                if(NoteViewModel.this.listener != null) {
                    NoteViewModel.this.listener.onNoteChange(note);
                }
            }
        });
        synonimes.observe(owner, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                if(NoteViewModel.this.listener != null) {
                    NoteViewModel.this.listener.onSynonimesChange(notes);
                }
            }
        });
        traductions.observe(owner, new Observer<List<Traduction>>() {
            @Override
            public void onChanged(List<Traduction> traductions) {
                if(NoteViewModel.this.listener != null) {
                    NoteViewModel.this.listener.onTraductionsChange(traductions);
                }
            }
        });
        close.observe(owner, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(NoteViewModel.this.listener != null) {
                    if(close.getValue() != null && close.getValue())
                        NoteViewModel.this.listener.onClose();
                }
            }
        });
    }

    private void iniLiveData() {
        if(note == null) {
            note = new MutableLiveData<>();
            note.setValue(new Note());
        }
        if(synonimes == null) {
            synonimes = new MutableLiveData<>();
            synonimes.setValue(new ArrayList<Note>());
        }
        if(traductions == null) {
            traductions = new MutableLiveData<>();
            traductions.setValue(new ArrayList<Traduction>());
        }
    }

    public Note getNote() {
        iniLiveData();
        Note note = this.note.getValue();
        if(note == null)
            return null;
        note.setTraductions(this.traductions.getValue());
        note.setSynonimes(this.synonimes.getValue());
        return note;
    }

    public void setNote(Note note) {
        iniLiveData();
        this.note.setValue(note);
        if(note.getSynonimes() != null)
            this.synonimes.setValue(note.getSynonimes());
        if(note.getTraductions() != null)
            this.traductions.setValue(note.getTraductions());
    }
    public void setNoteExpression(String expression) {
        iniLiveData();
        Note n = note.getValue();
        if(n != null) {
            n.setExpression(expression);
            note.setValue(n);
        }
    }
    public void setNoteCommentaire(String commentaire) {
        if(note == null) {
            note = new MutableLiveData<>();
            note.setValue(new Note("", "", null));
        }
        Note n = note.getValue();
        if(n != null) {
            n.setCommentaire(commentaire);
            note.setValue(n);
        }
    }
    public void setNoteCategory(Category category) {
        if(note == null) {
            note = new MutableLiveData<>();
        }
        Note n = note.getValue();
        if(n != null) {
            n.setCategory(category);
            note.setValue(n);
        }
    }


    public void addSynonimes(Note synonime) {
        iniLiveData();
        List<Note> s = this.synonimes.getValue();
        if(s != null) {
            s.add(0, synonime);
            this.synonimes.setValue(s);
        }
    }
    public void addTraductions(Traduction traduction) {
        iniLiveData();
        List<Traduction> t = this.traductions.getValue();
        if(t != null) {
            t.add(0, traduction);
            this.traductions.setValue(t);
        }
    }

    public boolean isAlreadyIn(Traduction traduction) {
        return this.traductions.getValue() != null && this.traductions.getValue().indexOf(traduction) > 0;
    }
    public boolean isAlreadyIn(Note synonime) {
        if(this.synonimes.getValue() == null)
            return false;
        List<Note> s = this.synonimes.getValue();
        int index = s.indexOf(synonime);
        return index >= 0;
    }

    public void deleteTraduction(Traduction traduction) {
        if(this.traductions != null) {
            List<Traduction> traductions = this.traductions.getValue();
            if(traductions != null) {
                traductions.remove(traduction);
                this.traductions.setValue(traductions);
            }
        }
    }
    public void deleteSynonime(Note synonime) {
        if(this.synonimes != null) {
            List<Note> synonimes = this.synonimes.getValue();
            if(synonimes != null) {
                synonimes.remove(synonime);
                this.synonimes.setValue(synonimes);
            }
        }
    }

    public void setClose() {
        if(close == null) {
            close = new MutableLiveData<>();
        }
        close.setValue(true);
    }

    public interface ChangeListener {
        void onNoteChange(Note note);
        void onSynonimesChange(List<Note> synonimes);
        void onTraductionsChange(List<Traduction> traductions);
        void onClose();
    }
}
