package com.gumeteapps.myenglishnotebook.database.entities;

import androidx.annotation.Nullable;

import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = "Categories")
public class Category implements Persistable {

    @DatabaseField(columnName = "id", generatedId = true, canBeNull = false, unique = true)
    private int id;
    @DatabaseField(columnName = "titre", defaultValue = "")
    private String titre;
    @DatabaseField(columnName = "description", defaultValue = "")
    private String description;

    private List<Note> notes;

    public Category() {
    }

    public Category(String titre, String description) {
        this.titre = titre;
        this.description = description;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj != null && obj.getClass() == Category.class) {
            Category c = (Category) obj;
            return this.id == c.id;
        }
        return super.equals(obj);
    }
}
