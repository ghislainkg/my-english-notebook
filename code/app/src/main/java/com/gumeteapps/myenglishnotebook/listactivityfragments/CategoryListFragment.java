package com.gumeteapps.myenglishnotebook.listactivityfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.gumeteapps.myenglishnotebook.CategoryPersister;
import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.othercomponents.DialogEditText;
import com.gumeteapps.myenglishnotebook.othercomponents.SnackBarPendingOperation;

import java.util.List;

/*Fragment qui affiche la liste des categories,
permet la creation de nouvelles categories,
 mise ajour de categories
 et supprission de categories*/
public class CategoryListFragment extends Fragment {

    // Le persister pour la communication avec la base de donnees
    private CategoryPersister persister;
    // L'adapter de la liste de categories
    private CategoryListAdapter adapter;
    // Le view model de l'activite parente contenant les categories (et les notes)
    private ListActivityViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_list_fragment, container, false);
        // On initialise le recycler View
        adapter = new CategoryListAdapter();
        adapter.setListener(getAdapterListener(view));
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewCategoryList);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setAdapter(adapter);
        // Et le bouton d'ajout de categories
        AppCompatImageButton button = view.findViewById(R.id.floatingActionButtonCategoryList);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(null);
            }
        });
        return view;
    }

    // Cette fonction lance un flow de creation de categories avec des dialogs
    // Le premier dialog permet d'editer un titre quand il est valide, il lance le second dialog
    // qui demande la description de la categorie
    private void edit(final Category toUpdate) {
        DialogEditText.Listener titreDialogListener = new DialogEditText.Listener() {
            @Override
            public void onOk(final String titre) {
                DialogEditText descriptionDialog = new DialogEditText(getString(R.string.str_category_description));
                descriptionDialog.setListener(new DialogEditText.Listener() {
                    @Override
                    public void onOk(String description) {
                        if(toUpdate == null) {
                            Category category = new Category(titre, description);
                            persister.persist(category);
                            viewModel.addCategory(category);
                        }
                        else {
                            viewModel.removeCategory(toUpdate);
                            persister.update(toUpdate);
                            viewModel.addCategory(toUpdate);
                        }
                    }
                });
                descriptionDialog.show(getChildFragmentManager(), "create-category-description");
            }
        };
        DialogEditText titreDialog = new DialogEditText(getString(R.string.str_category_titre));
        titreDialog.setListener(titreDialogListener);
        titreDialog.show(getParentFragmentManager(), "create-category-titre");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        persister = new CategoryPersister(requireActivity());
        viewModel = ViewModelProviders.of(requireActivity()).get(ListActivityViewModel.class);
        viewModel.getCategoriesData().observe(requireActivity(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                adapter.setCategories(categories);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // On syncronise l'affichage et le viewModel avec la base de donnees
        viewModel.setCategories(persister.getAll());
    }

    private CategoryListAdapter.Listener getAdapterListener(final View view) {
        return new CategoryListAdapter.Listener() {

            @Override
            public void onClick(Category category) {
                edit(category);
            }

            @Override
            public void onDelete(final Category category) {
                if (category.getTitre().equals(persister.getGlobal().getTitre())) {
                    Toast.makeText(requireActivity(), R.string.msg_cant_delete_global, Toast.LENGTH_SHORT).show();
                }
                SnackBarPendingOperation.snack(
                        view.findViewById(R.id.layoutCategoryList),
                        getString(R.string.msg_pedding_category_Delete),
                        new SnackBarPendingOperation.OnDismissListener() {
                            @Override
                            public void onAccepted() {
                                persister.deleteCategory(category);
                                viewModel.removeCategory(category);
                            }
                        });
            }
        };
    }

}
