package com.gumeteapps.myenglishnotebook.database.entities;

import androidx.annotation.NonNull;

import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "assos_synonime")
public class AssosSynonime implements Persistable {
    @DatabaseField(columnName = "id", generatedId = true, canBeNull = false, unique = true)
    private int id;
    @DatabaseField(columnName = "note", foreign = true, foreignColumnName = "id")
    private Note note;
    @DatabaseField(columnName = "synonime", foreign = true, foreignColumnName = "id")
    private Note synonime;

    public AssosSynonime() {
    }

    public AssosSynonime(Note note, Note synonime) {
        this.note = note;
        this.synonime = synonime;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return "Association";
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Note getSynonime() {
        return synonime;
    }

    public void setSynonime(Note synonime) {
        this.synonime = synonime;
    }

    @NonNull
    @Override
    public String toString() {
        return "AssosSynonime{" +
                "id=" + id +
                ", note=" + note +
                ", synonime=" + synonime +
                '}';
    }
}
