package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gumeteapps.myenglishnotebook.CategoryPersister;
import com.gumeteapps.myenglishnotebook.NoteViewModel;
import com.gumeteapps.myenglishnotebook.NotePersister;
import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;
import com.gumeteapps.myenglishnotebook.othercomponents.AdapterMiniPersistableListDeletable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*L'ajout de synonimes pour notre note*/
public class NoteEditorFragmentSynonime extends Fragment {

    /*Le recyclerView d'affichage des synonimes et son adapter
    * Ils Permettent la suppression de synonime*/
    private RecyclerView synonimeList;
    private AdapterMiniPersistableListDeletable synonimeAdapter;
    /*La vue de saisie de synonime*/
    private TextInputEditText inputEditText;
    // Le viewModel
    private NoteViewModel viewModel;

    public NoteEditorFragmentSynonime() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_editor_synonime_fragment, container, false);
        // On recupere les vues
        Button terminer = view.findViewById(R.id.buttonTerminerNoteSynonime);
        Button abandonner = view.findViewById(R.id.buttonAbandonnerNoteSynonime);
        Button autreSynonime = view.findViewById(R.id.buttonSynonimeNoteSynonime);
        Button choisirNote = view.findViewById(R.id.buttonNoteNoteSynonime);
        synonimeList = view.findViewById(R.id.recyclerViewNoteSynonime);
        inputEditText = view.findViewById(R.id.textInputEditNoteSynonime);
        // On leur assigne les listeners
        terminer.setOnClickListener(OnTerminer);
        abandonner.setOnClickListener(OnAbandonner);
        autreSynonime.setOnClickListener(OnAutreSynonime);
        choisirNote.setOnClickListener(OnChoisirNote);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // voir NoteEditorFragmentTraduction (meme logique)
        inputEditText.setText("");
        // voir NoteEditorFragmentTraduction (meme logique)
        synonimeAdapter = new AdapterMiniPersistableListDeletable(onDeleteSynonime);
        synonimeList.setLayoutManager(new LinearLayoutManager(requireContext()));
        synonimeList.setAdapter(synonimeAdapter);
        // On recupere le viewModel de la classe parente
        viewModel = ViewModelProviders.of(requireActivity()).get(NoteViewModel.class);
        viewModel.listen(requireActivity(), changeListener);
    }

    private View.OnClickListener OnTerminer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addSynonime();
            // On rend la note et ses traductions et synonimes persistant
            NotePersister persister = new NotePersister(requireActivity());
            persister.persist(viewModel.getNote());
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnAbandonner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // On ferme l'activite
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnAutreSynonime = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addSynonime();
            inputEditText.setText("");
        }
    };

    private View.OnClickListener OnChoisirNote = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            NotePersister persister = new NotePersister(requireActivity());
            NoteDialogChooseSynonime dialog = NoteDialogChooseSynonime.newInstance(persister.getAll());
            dialog.setOnChooseSynonimeListener(onChooseSynonime);
            dialog.show(getParentFragmentManager(), "choisir-synonime");
        }
    };
    private NoteDialogChooseSynonime.OnChooseSynonimeListener onChooseSynonime =
            new NoteDialogChooseSynonime.OnChooseSynonimeListener() {
                @Override
                public void onChoose(Note synonime) {
                    if(viewModel.isAlreadyIn(synonime)) {
                        Toast.makeText(requireActivity(), R.string.msg_synonime_alreadyin, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    viewModel.addSynonimes(synonime);
                }
                @Override
                public void onCancel() { }
            };
    private AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener onDeleteSynonime =
            new AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener() {
                @Override
                public void onClick(Persistable p) { }

                @Override
                public void onDelete(Persistable p) {
                    viewModel.deleteSynonime((Note)p);
                }
            };

    /*Meme duo que dans NoteEditorFragmentTraduction*/
    private NoteViewModel.ChangeListener changeListener = new NoteViewModel.ChangeListener() {
        @Override
        public void onNoteChange(Note note) { }
        @Override
        public void onClose() {}
        @Override
        public void onSynonimesChange(List<Note> synonimes) {
            if(synonimes != null)
                synonimeAdapter.setData(new ArrayList<Persistable>(synonimes));
        }

        @Override
        public void onTraductionsChange(List<Traduction> traductions) { }
    };
    private void addSynonime() {
        String text = Objects.requireNonNull(inputEditText.getText()).toString();
        if (!text.isEmpty()) {
            CategoryPersister persister = new CategoryPersister(requireActivity());
            Note n = new Note(text, "", persister.getGlobal());
            viewModel.addSynonimes(n);
        }
    }
}
