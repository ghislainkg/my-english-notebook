package com.gumeteapps.myenglishnotebook.othercomponents;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gumeteapps.myenglishnotebook.database.Persistable;

import java.util.ArrayList;
import java.util.List;

import com.gumeteapps.myenglishnotebook.R;

public class AdapterMiniPersistableList extends RecyclerView.Adapter<AdapterMiniPersistableList.ViewHolder> {

    private static final String TAG = "mDebug";

    private List<Persistable> data;
    private MiniPersistableListListener listener;

    public AdapterMiniPersistableList() {
        this.data = new ArrayList<>();
    }
    public AdapterMiniPersistableList(MiniPersistableListListener listener) {
        this.listener = listener;
        this.data = new ArrayList<>();
    }
    public AdapterMiniPersistableList(List<Persistable> persistables, MiniPersistableListListener listener) {
        this.data = persistables;
        this.listener = listener;
    }

    public List<Persistable> getData() {
        return data;
    }

    public void setData(List<Persistable> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    public void setListener(MiniPersistableListListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.mini_persistable_list_view_holder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.display(data.get(position));
    }

    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCount: "+data.size());
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        Persistable maData;
        ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textViewMiniPersistableListViewHolder);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        listener.onClick(maData);
                    }
                }
            });
        }

        void display(Persistable p) {
            textView.setText(p.getTitle());
            this.maData = p;
        }
    }

    public interface MiniPersistableListListener {
        void onClick(Persistable p);
    }
}
