package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.othercomponents.AdapterMiniPersistableList;

import java.util.ArrayList;
import java.util.List;

public class NoteDialogChooseSynonime extends DialogFragment {

    private OnChooseSynonimeListener onChooseSynonimeListener;
    private List<Note> synonimes;

    public NoteDialogChooseSynonime() { }
    public static NoteDialogChooseSynonime newInstance(List<Note> synonimes) {
        NoteDialogChooseSynonime res = new NoteDialogChooseSynonime();
        res.synonimes = synonimes;
        return res;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.note_dialog_choose_synonime_fragment, null);

        initRecyclerView(view);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onChooseSynonimeListener.onCancel();
                    }
                });

        return builder.create();
    }

    private void initRecyclerView(View view) {
        AdapterMiniPersistableList adapter = new AdapterMiniPersistableList(
                new ArrayList<Persistable>(this.synonimes),
                adapterListener);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewDialogChooseSynonime);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    private AdapterMiniPersistableList.MiniPersistableListListener adapterListener =
            new AdapterMiniPersistableList.MiniPersistableListListener() {
                @Override
                public void onClick(Persistable p) {
                    onChooseSynonimeListener.onChoose((Note) p);
                    dismiss();
                }
            };

    public void setOnChooseSynonimeListener(OnChooseSynonimeListener onChooseSynonimeListener) {
        this.onChooseSynonimeListener = onChooseSynonimeListener;
    }

    public interface OnChooseSynonimeListener{
        void onChoose(Note synonime);
        void onCancel();
    }
}
