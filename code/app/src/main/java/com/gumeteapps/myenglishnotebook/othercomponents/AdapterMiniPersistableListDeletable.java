package com.gumeteapps.myenglishnotebook.othercomponents;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.Persistable;

import java.util.ArrayList;
import java.util.List;

public class AdapterMiniPersistableListDeletable extends RecyclerView.Adapter<AdapterMiniPersistableListDeletable.ViewHolder> {

    private List<Persistable> data;
    private MiniPersistableListDeletableListener listener;

    public AdapterMiniPersistableListDeletable() {
        data = new ArrayList<>();
    }
    public AdapterMiniPersistableListDeletable(MiniPersistableListDeletableListener listener) {
        this.listener = listener;
        data = new ArrayList<>();
    }
    public AdapterMiniPersistableListDeletable(List<Persistable> persistables, MiniPersistableListDeletableListener listener) {
        this.data = persistables;
        this.listener = listener;
    }

    public List<Persistable> getData() {
        return data;
    }

    public void setData(List<Persistable> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    public void setListener(MiniPersistableListDeletableListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterMiniPersistableListDeletable.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.mini_persistable_list_delete_view_holder, parent, false);
        return new AdapterMiniPersistableListDeletable.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMiniPersistableListDeletable.ViewHolder holder, int position) {
        holder.display(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        Button delete;
        Persistable maData;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textViewMiniPersistableListDeleteViewHolder);
            delete = itemView.findViewById(R.id.buttonMiniPersistableListDeleteViewHolder);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(maData);
                    }
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        listener.onDelete(maData);
                    }
                }
            });
        }

        void display(Persistable p) {
            textView.setText(p.getTitle());
            this.maData = p;
        }
    }

    public interface MiniPersistableListDeletableListener {
        void onClick(Persistable p);
        void onDelete(Persistable p);
    }

}
