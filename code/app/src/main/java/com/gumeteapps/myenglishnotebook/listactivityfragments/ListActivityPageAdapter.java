package com.gumeteapps.myenglishnotebook.listactivityfragments;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ListActivityPageAdapter extends FragmentStatePagerAdapter {

    public ListActivityPageAdapter(@NonNull FragmentManager fm) {
        super(fm, ListActivityPageAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NoteListFragment();
            case 1:
                return new CategoryListFragment();
        }
        return new NoteListFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}
