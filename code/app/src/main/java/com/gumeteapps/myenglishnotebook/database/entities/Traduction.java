package com.gumeteapps.myenglishnotebook.database.entities;

import androidx.annotation.Nullable;

import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Traductions")
public class Traduction implements Persistable {

    @DatabaseField(columnName = "id", generatedId = true, canBeNull = false, unique = true)
    private int id;
    @DatabaseField(columnName = "texte", defaultValue = "")
    private String texte;
    @DatabaseField(columnName = "note", foreign = true, foreignColumnName = "id")
    private Note note;

    public Traduction() {
    }

    public Traduction(String texte, Note note) {
        this.texte = texte;
        this.note = note;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return texte;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Traduction{" +
                "id=" + id +
                ", texte='" + texte + '\'' +
                ", note=" + note +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj != null && obj.getClass() == Traduction.class) {
            Traduction t = (Traduction) obj;
            return t.id == this.id;
        }
        return super.equals(obj);
    }
}
