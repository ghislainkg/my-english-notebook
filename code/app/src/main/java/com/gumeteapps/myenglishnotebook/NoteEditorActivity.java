package com.gumeteapps.myenglishnotebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.util.Log;

import com.gumeteapps.myenglishnotebook.database.DataBase;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;

import java.util.List;

public class NoteEditorActivity extends AppCompatActivity {
    public static final String EXTRA_NOTE = "note";

    private static final String TAG = "mDebug";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_editor_activity);

        NavController navController = Navigation.findNavController(this, R.id.navHostNoteEditor);
        Toolbar toolbar = findViewById(R.id.toolbarNoteEditor);
        NavigationUI.setupWithNavController(toolbar, navController);

        NoteViewModel viewModel = ViewModelProviders.of(this).get(NoteViewModel.class);

        if(getIntent().hasExtra(EXTRA_NOTE)) {
        }
        else {
            DataBase dataBase = new DataBase(this);
            viewModel.setNote(new Note("", "",dataBase.getGlobalCategory()));
        }
        viewModel.listen(this, viewModelListener);
    }

    private NoteViewModel.ChangeListener viewModelListener = new NoteViewModel.ChangeListener() {
        @Override
        public void onNoteChange(Note note) { }
        @Override
        public void onSynonimesChange(List<Note> synonimes) { }
        @Override
        public void onTraductionsChange(List<Traduction> traductions) { }
        @Override
        public void onClose() {
            finish();
        }
    };
}
