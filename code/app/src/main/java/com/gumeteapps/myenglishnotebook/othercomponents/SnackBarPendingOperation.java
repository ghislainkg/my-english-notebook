package com.gumeteapps.myenglishnotebook.othercomponents;

import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.gumeteapps.myenglishnotebook.R;

public class SnackBarPendingOperation {

    public static void snack(View view, String text, final OnDismissListener listener) {
        final boolean[] deleteAccepted = new boolean[1];
        deleteAccepted[0] = true;
        Snackbar snackbar = Snackbar.make(
                view,
                text,
                Snackbar.LENGTH_SHORT);
        snackbar.setAction(
                R.string.str_annuler,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // L'utilisateur peut empecher la suppression
                        deleteAccepted[0] = false;
                    }
                });
        snackbar.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                Log.i("mDebug", "onAccepted:");
                if(deleteAccepted[0]) {
                    listener.onAccepted();
                }
            }
        });
        snackbar.show();
    }

    public interface OnDismissListener {
        void onAccepted();
    }
}
