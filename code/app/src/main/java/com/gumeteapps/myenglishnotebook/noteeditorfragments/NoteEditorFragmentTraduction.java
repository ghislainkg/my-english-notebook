package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.gumeteapps.myenglishnotebook.NoteViewModel;
import com.gumeteapps.myenglishnotebook.NotePersister;
import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;
import com.gumeteapps.myenglishnotebook.othercomponents.AdapterMiniPersistableListDeletable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*Etape de l'edition de note dans laquelle l'utilisateur ajoute de multiple traductions*/
public class NoteEditorFragmentTraduction extends Fragment {

    // Le recyclerView affichant les traductions ajoutees et son adapter
    // (ils permettent la suppression de traductions)
    private RecyclerView traductionsList;
    private AdapterMiniPersistableListDeletable traductionAdapter;
    // La vue de saisie de traduction
    private TextInputEditText inputEditText;
    // Le viewModel de l'activite parente, contient le informations de la note en cours d'edition
    private NoteViewModel viewModel;
    // La navigation
    private NavController navController;

    public NoteEditorFragmentTraduction() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_editor_traduction_fragment, container, false);
        // On recupere toute les vues
        Button continuer = view.findViewById(R.id.buttonContinueNoteTraduction);
        Button terminer = view.findViewById(R.id.buttonTerminerNoteTraduction);
        Button abandonner = view.findViewById(R.id.buttonAbandonnerNoteTraduction);
        Button autreTraduction = view.findViewById(R.id.buttonTraductionNoteTraduction);
        traductionsList = view.findViewById(R.id.recyclerViewNoteTraduction);
        inputEditText = view.findViewById(R.id.textInputEditNoteTraduction);
        // On ajoute leur listener
        continuer.setOnClickListener(OnContinue);
        terminer.setOnClickListener(OnTerminer);
        abandonner.setOnClickListener(OnAbandonner);
        autreTraduction.setOnClickListener(OnAutreTraduction);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // On recupere le navigation controller
        navController = Navigation.findNavController(view);
        // On n'affiche rien comme traduction.
        inputEditText.setText("");
        // On creer la liste de traduction, elle sera mise a jour grace a l'ecoute du viewModel ci dessus.
        // L'activite parente se charge de mettre des tradcutions dans le view model
        // en fonction des extra qu'elle recoit.
        // cette liste est mise a jour a chaque fois qu'il y a une nouvelle traduction dans le viewModel
        // (Ecoute via changeListener)
        traductionAdapter = new AdapterMiniPersistableListDeletable(
                //On ecoute le click sur le bouton delete/supprimer de la liste
                onDeleteTraduction);
        traductionsList.setLayoutManager(new LinearLayoutManager(requireContext()));
        traductionsList.setAdapter(traductionAdapter);
        // On recupere le viewModel de l'activite parente
        // On observe les modification qu'il subit
        // (A priori, toutes ces modifications viendrons de ce meme fragment)
        viewModel = ViewModelProviders.of(requireActivity()).get(NoteViewModel.class);
        viewModel.listen(requireActivity(), changeListener);
    }

    // LES PRINCIPAUX LISTENERS
    private View.OnClickListener OnContinue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut aller a l'etape suivante
            // On ajoute la traduction que contient la vue de saisie au viewModel
            addTraduction();
            //Puis on vas a l'ajout de synonimes
            navController.navigate(R.id.action_noteEditorFragmentTraduction_to_noteEditorFragmentSynonime);
        }
    };
    private View.OnClickListener OnTerminer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut terminer l'edition de la note.
            // On ajoute la traduction contenu dans la vue de saisie au viewModel
            // (si elle n'est pas vide bien-sur)
            addTraduction();
            // On enregistre la note dans la base de donnee
            NotePersister persister = new NotePersister(requireActivity());
            persister.persist(viewModel.getNote());
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnAbandonner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut abandonner l'edition de la note
            // On ferme l'activite
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnAutreTraduction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut ajouter une autre traduction
            // On ajoute la traductionactuellement dans la vue de saisie
            // (elle est ajoute au viewModel et a la liste de traductions)
            addTraduction();
            // On laisse la place pour une nouvelle saisi
            inputEditText.setText("");
        }
    };
    private AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener onDeleteTraduction =
    new AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener() {
        @Override
        public void onClick(Persistable p) { }

        @Override
        public void onDelete(Persistable p) {
            // L'utilisateur veut supprimer une traduction de la liste
            // On la supprime du viewModel. la liste est mise a jour
            viewModel.deleteTraduction((Traduction) p);
        }
    };

    // LE COUPLE changeListener X addTraduction
    // Lorsque addTraduction est appellee, elle creer une nouvelle traduction a partir du texte de
    // la vue de saisie (si elle n'est pas vide) et l'ajoute au viewModel. changeListener ecoute se changement du viewModel
    // et met a jour la liste de traduction.
    // Il en rresuslte que chaque appel de addTraduction met ajour le viewModel et la liste (le recyclerView/Adapter)
    private NoteViewModel.ChangeListener changeListener = new NoteViewModel.ChangeListener() {
        @Override
        public void onNoteChange(Note note) { }
        @Override
        public void onClose() {}
        @Override
        public void onSynonimesChange(List<Note> synonimes) { }

        @Override
        public void onTraductionsChange(List<Traduction> traductions) {
            if(traductions != null)
                traductionAdapter.setData(new ArrayList<Persistable>(traductions));
        }
    };
    private void addTraduction() {
        String text = Objects.requireNonNull(inputEditText.getText()).toString();
        if(!text.isEmpty()) {
            Traduction traduction = new Traduction(text, null);
            viewModel.addTraductions(traduction);
        }
    }
}
