package com.gumeteapps.myenglishnotebook.database;

import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;

import java.util.Random;

public class TestData {
    private DataBase dataBase;

    public TestData(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public void generate() {
        for (int i = 0; i < 20; i++) {
            Category c = new Category("Categorie "+i, "category description, yes this is a category description");
            dataBase.create(c);

            Random r = new Random();
            r.nextInt(20);
            for (int j = 0; j < 50; j++) {
                Note n = new Note("Expression"+j, "This is a comment, yes a comment", c);
                dataBase.create(n);
            }
        }
    }
}
