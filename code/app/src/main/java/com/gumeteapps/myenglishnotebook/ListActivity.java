package com.gumeteapps.myenglishnotebook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.gumeteapps.myenglishnotebook.listactivityfragments.ListActivityPageAdapter;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        ListActivityPageAdapter pageAdapter = new ListActivityPageAdapter(getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.viewPagerListActivity);
        viewPager.setAdapter(pageAdapter);

        showMessageDialog();
    }

    private void showMessageDialog() {

        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE);
        boolean display = sharedPref.getBoolean(getString(R.string.key_value_display_message), true);

        if(!display)
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.swipe_message);
        builder.setPositiveButton(R.string.dont_show_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.key_value_display_message), false);
                editor.apply();
            }
        });
        builder.create().show();
    }
}
