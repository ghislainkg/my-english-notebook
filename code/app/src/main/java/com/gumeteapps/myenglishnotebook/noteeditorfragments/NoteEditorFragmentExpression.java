package com.gumeteapps.myenglishnotebook.noteeditorfragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gumeteapps.myenglishnotebook.CategoryPersister;
import com.gumeteapps.myenglishnotebook.NoteViewModel;
import com.gumeteapps.myenglishnotebook.NotePersister;
import com.gumeteapps.myenglishnotebook.R;
import com.gumeteapps.myenglishnotebook.database.entities.Category;

import java.util.Objects;


/*La premier etape de l'edition d'une nouvelle note
* Ajoute une expression et une categori pour la note*/
public class NoteEditorFragmentExpression extends Fragment {

    // La vue de saisie de l'expression
    private TextInputEditText inputEditText;
    // Le bouton pour la categorie. affiche la categorie courante
    private Button choisirCategory;
    // Le view model contient toute les donnees de la note en cours d'edition
    private NoteViewModel viewModel;
    // la navigation
    private NavController navController;

    public NoteEditorFragmentExpression() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_editor_expression_fragment, container, false);
        // On recupere toutes les vues
        Button continuer = view.findViewById(R.id.buttonContinueNoteExpression);
        Button terminer = view.findViewById(R.id.buttonTerminerNoteExpression);
        Button abandonner = view.findViewById(R.id.buttonAbandonnerNoteExpression);
        choisirCategory = view.findViewById(R.id.buttonCategoryNoteExpression);
        inputEditText = view.findViewById(R.id.textInputEditNoteExpression);
        // On ajoute les listener pour chaque boutons
        continuer.setOnClickListener(OnContinue);
        terminer.setOnClickListener(OnTerminer);
        abandonner.setOnClickListener(OnAbandonner);
        choisirCategory.setOnClickListener(OnChoisirCategory);

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // On recupere le viewModel de l'acitite parente et le navigation controller
        viewModel = ViewModelProviders.of(requireActivity()).get(NoteViewModel.class);
        navController = Navigation.findNavController(view);
    }
    @Override
    public void onResume() {
        super.onResume();
        // On prerempli l'expression et la categorie a partir du viewModel
        // L'activite parente s'occupe de mettre une expression en fonction des extra qu'elle recoit
        inputEditText.setText(viewModel.getNote().getExpression());
        choisirCategory.setText(viewModel.getNote().getCategory().getTitre());
    }

    // LES LISTENER PRINCIPAUX
    private View.OnClickListener OnContinue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut continuer l'edition de la note.
            // On ajoute l'expression au viewModel si elle n'est pas vide.
            // Dans le cas contraire, on ne vas nul part
            // Le bouton choisir s'occupe de mettre la categorie dans le viewModel
            String text = Objects.requireNonNull(inputEditText.getText()).toString();
            if(text.isEmpty()) {
                // Donc s'il n'y a pas d'expression, on reste la
                Toast.makeText(requireActivity(), getString(R.string.mdg_need_expression), Toast.LENGTH_SHORT).show();
            }
            else {
                // Par contre s'il y en a, on met a jour le viewModel
                viewModel.setNoteExpression(text);
                // Puis on vas aux traductions
                navController.navigate(R.id.action_noteEditorFragmentExpression_to_noteEditorFragmentCommentaire);
            }
        }
    };
    private View.OnClickListener OnTerminer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veux terminer la l'edition de sa note
            // Donc aura une note contenant une categorie (Global ou autre) et une expression.
            // On ajoute d'abord l'expression au viewModel.
            String text = Objects.requireNonNull(inputEditText.getText()).toString();
            if(text.isEmpty()) {
                // s'il n'y a pas d'expression, on ne vas nul part
                Toast.makeText(requireActivity(), getString(R.string.mdg_need_expression), Toast.LENGTH_SHORT).show();
                return;
            }
            else
                // Si non, on ajoute l'expression au viewModel
                viewModel.setNoteExpression(text);
            // Puis on enregistre le contenu du viewModel dans la base de donnees
            NotePersister persister = new NotePersister(requireActivity());
            persister.persist(viewModel.getNote());
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnAbandonner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur abandonne l'edition de notecourante
            // On ferme l'activite
            requireActivity().finish();
        }
    };
    private View.OnClickListener OnChoisirCategory = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut choisir une autre categorie
            // On affiche un dialog de selection de categorie
            // Le onChooseCategory juste en dessous recoit la reponse de l'utilisateur
            // et l'ajoute (s'il y en a une) au viewModel
            CategoryPersister persister = new CategoryPersister(requireActivity());
            NoteDialogChooseCategorie dialog = NoteDialogChooseCategorie.newInstance(persister.getAll());
            dialog.setOnChooseCategoryListener(onChooseCategory);
            dialog.show(getParentFragmentManager(), "choisir-category");
        }
    };
    private NoteDialogChooseCategorie.OnChooseCategoryListener onChooseCategory= new NoteDialogChooseCategorie.OnChooseCategoryListener() {
        @Override
        public void onChoose(Category category) {
            // L'utilisateur a choisi une categorie
            // On l'ajoute au view model et on met a jour l'affichege du bouton choisir categorie
            viewModel.setNoteCategory(category);
            choisirCategory.setText(category.getTitre());
        }

        @Override
        public void onCancel() { }
    };
}
