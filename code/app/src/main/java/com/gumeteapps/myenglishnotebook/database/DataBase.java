package com.gumeteapps.myenglishnotebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.gumeteapps.myenglishnotebook.database.entities.AssosSynonime;
import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import com.gumeteapps.myenglishnotebook.R;
import com.j256.ormlite.table.TableUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataBase extends OrmLiteSqliteOpenHelper {
    public static final String databaseName = "Notebook";
    public static final int databaseVersion = 14;
    private Context context;

    private static final String TAG = "mdebug";

    public DataBase(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource){
        try {
            TableUtils.createTable(connectionSource, Note.class);
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Traduction.class);
            TableUtils.createTable(connectionSource, AssosSynonime.class);
            // Creation de la categorie GLOBAL
            createGlocalCategory();

            // Test data
            //TestData testData = new TestData(this);
            //testData.generate();
        }
        catch (Exception e) {
            Log.e(TAG, "onCreate: ", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, AssosSynonime.class, true);
            TableUtils.dropTable(connectionSource, Traduction.class, true);
            TableUtils.dropTable(connectionSource, Note.class, true);
            TableUtils.dropTable(connectionSource, Category.class, true);
            onCreate(database, connectionSource);
        } catch (Exception e) {
            Log.e(TAG, "onUpgrade: ", e);
        }
    }

    private void createGlocalCategory() {
        try {
            Dao<Category, Integer> dao = getDao(Category.class);
            dao.create(new Category("GLOBAL", "GLOBAL"));
        }catch (Exception e) {
            Log.e(TAG, "onCreate: Fail to create categorie GLOBAL", e);
        }
    }

    public Category getGlobalCategory() throws SQLiteException {
        try {
            for (int i = 0; i < 5; i++) {
                Dao<Category, Integer> dao = getDao(Category.class);
                List<Category> res = dao.queryForEq("description", "GLOBAL");
                if(res.size() >= 1)
                    return res.get(0);
                else {
                    createGlocalCategory();
                }
            }
            throw new SQLiteException(context.getString(R.string.error_database));
        }catch (Exception e) {
            Log.e(TAG, "getGlobalCategory: ", e);
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }

    public void create(Persistable persistable) throws SQLiteException {
        try {
            if(persistable.getClass() == Note.class) {
                Dao<Note, Integer> dao = getDao(Note.class);
                dao.create((Note) persistable);
            }
            else if(persistable.getClass() == Category.class) {
                Dao<Category, Integer> dao = getDao(Category.class);
                dao.create((Category) persistable);
            }
            else if(persistable.getClass() == Traduction.class) {
                Dao<Traduction, Integer> dao = getDao(Traduction.class);
                dao.create((Traduction) persistable);
            }
            else if(persistable.getClass() == AssosSynonime.class) {
                Dao<AssosSynonime, Integer> dao = getDao(AssosSynonime.class);
                dao.create((AssosSynonime) persistable);
            }
            else {
                Log.e(TAG, "create: Attempt to create no persistable object in database", null);
            }
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public void delete(Persistable persistable) throws SQLiteException {
        try {
            if(persistable.getClass() == Note.class) {
                Dao<Note, Integer> dao = getDao(Note.class);
                dao.delete((Note) persistable);
            }
            else if(persistable.getClass() == Category.class) {
                Dao<Category, Integer> dao = getDao(Category.class);
                dao.delete((Category) persistable);
            }
            else if(persistable.getClass() == Traduction.class) {
                Dao<Traduction, Integer> dao = getDao(Traduction.class);
                dao.delete((Traduction) persistable);
            }
            else if(persistable.getClass() == AssosSynonime.class) {
                Dao<AssosSynonime, Integer> dao = getDao(AssosSynonime.class);
                dao.delete((AssosSynonime) persistable);
            }
            else {
                Log.e(TAG, "create: Attempt to create no persistable object in database", null);
            }
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public void update(Persistable persistable) throws SQLiteException {
        try {
            if(persistable.getClass() == Note.class) {
                Dao<Note, Integer> dao = getDao(Note.class);
                dao.createOrUpdate((Note) persistable);
            }
            else if(persistable.getClass() == Category.class) {
                Dao<Category, Integer> dao = getDao(Category.class);
                dao.createOrUpdate((Category) persistable);
            }
            else if(persistable.getClass() == Traduction.class) {
                Dao<Traduction, Integer> dao = getDao(Traduction.class);
                dao.createOrUpdate((Traduction) persistable);
            }
            else if(persistable.getClass() == AssosSynonime.class) {
                Dao<AssosSynonime, Integer> dao = getDao(AssosSynonime.class);
                dao.createOrUpdate((AssosSynonime) persistable);
            }
            else {
                Log.e(TAG, "create: Attempt to create no persistable object in database", null);
            }
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }



    public Note getNoteById(int id) throws SQLiteException {
        try {
            Dao<Note, Integer> dao = getDao(Note.class);
            return dao.queryForId(id);
        }catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public Category getCategoryById(int id) throws SQLiteException {
        try {
            Dao<Category, Integer> dao = getDao(Category.class);
            return dao.queryForId(id);
        }catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public Traduction getTraductionById(int id) throws SQLiteException {
        try {
            Dao<Traduction, Integer> dao = getDao(Traduction.class);
            return dao.queryForId(id);
        }catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }


    public List<Note> getAllNotes() throws SQLiteException {
        try {
            Dao<Note, Integer> dao = getDao(Note.class);
            return dao.queryForAll();
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public List<Category> getAllCategories() throws SQLiteException {
        try {
            Dao<Category, Integer> dao = getDao(Category.class);
            return dao.queryForAll();
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public List<Traduction> getAllTraductions() throws SQLiteException {
        try {
            Dao<Traduction, Integer> dao = getDao(Traduction.class);
            return dao.queryForAll();
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }


    public List<AssosSynonime> getAssosSynonime(Note note) throws SQLiteException {
        try {
            Dao<AssosSynonime, Integer> dao = getDao(AssosSynonime.class);
            return dao.queryForEq("note", note);
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public List<Traduction> getTraductionsByNote(Note note) throws SQLiteException {
        try {
            Dao<Traduction, Integer> dao = getDao(Traduction.class);
            return dao.queryForEq("note", note);
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }
    public List<Note> getNoteByCategory(Category category) throws SQLiteException {
        try {
            Dao<Note, Integer> dao = getDao(Note.class);
            return dao.queryForEq("category", category);
        } catch (Exception e) {
            throw new SQLiteException(context.getString(R.string.error_database));
        }
    }

    public void removeSynonimeFrom(Note note, Note synonime) throws SQLiteException {
        try {
            this.getReadableDatabase().execSQL(
                    "DELETE FROM assos_synonime " +
                    "WHERE note = "+note.getId()+
                    " AND synonime = "+synonime.getId()+
                    ";"
            );
        }
        catch (Exception e) {
            Log.e(TAG, "removeSynonimeFrom: ", e);
            //throw new SQLiteException(context.getString(R.string.error_database));
        }
    }

    public void addSynonimeToNote(Note note, Note synonime) {
        try {
            Dao<AssosSynonime, Integer> dao = getDao(AssosSynonime.class);
            Map<String, Object> args = new HashMap<String, Object>();
            args.put("note", note);
            args.put("synonime", synonime);
            List<AssosSynonime> assos = dao.queryForFieldValues(args);
            if(assos.size() == 0) {
                dao.create(new AssosSynonime(note, synonime));
            }
        }
        catch (Exception e) {
            Log.e(TAG, "addSynonimeToNote: ", e);
        }
    }
}
