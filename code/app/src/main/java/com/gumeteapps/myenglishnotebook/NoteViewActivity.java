package com.gumeteapps.myenglishnotebook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gumeteapps.myenglishnotebook.database.DataBase;
import com.gumeteapps.myenglishnotebook.database.Persistable;
import com.gumeteapps.myenglishnotebook.database.entities.Category;
import com.gumeteapps.myenglishnotebook.database.entities.Note;
import com.gumeteapps.myenglishnotebook.database.entities.Traduction;
import com.gumeteapps.myenglishnotebook.noteeditorfragments.NoteDialogChooseCategorie;
import com.gumeteapps.myenglishnotebook.noteeditorfragments.NoteDialogChooseSynonime;
import com.gumeteapps.myenglishnotebook.othercomponents.DialogEditText;
import com.gumeteapps.myenglishnotebook.othercomponents.AdapterMiniPersistableListDeletable;
import com.gumeteapps.myenglishnotebook.othercomponents.SnackBarPendingOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*Cette activite affiche une note dont elle recoit l'id de la base de donnnees*/
public class NoteViewActivity extends AppCompatActivity {

    private static final String TAG = "mDebug";

    // Toutes les vues
    private TextView txtvExpression;
    private Button btnEditExpression;
    private TextView txtvComment;
    private Button btnEditComment;
    private TextView txtvCategory;
    private Button btnEditCategory;
    private Button btnAddTraduction;
    private Button btnAddSynonime;
    // Les adapter de la liste de synonimes et de tradcutions
    private AdapterMiniPersistableListDeletable adapterSynonime;
    private AdapterMiniPersistableListDeletable adapterTraduction;
    // Le nom de l'extra permettant de passer l'id de la note a l'activite
    public static final String EXTRA_NOTE = "note";
    // Le view model contenant la note
    private NoteViewModel viewModel;
    // Les persisters pour communiquer avec la base de donnees
    private NotePersister notePersister;
    private CategoryPersister categoryPersister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: Start");
        setContentView(R.layout.note_view_activity);
        // On recupere les textview et boutons
        txtvExpression = findViewById(R.id.textViewExpressionNoteView);
        btnEditExpression = findViewById(R.id.buttonExpressionEditNoteView);
        txtvComment = findViewById(R.id.textViewCommentNoteView);
        btnEditComment = findViewById(R.id.buttonCommentEditNoteView);
        txtvCategory = findViewById(R.id.textViewCategoryTitreNoteView);
        btnEditCategory = findViewById(R.id.buttonCategoryEditNoteView);
        btnAddTraduction = findViewById(R.id.buttonTraductionAddNoteView);
        btnAddSynonime = findViewById(R.id.buttonSynonimeAddNoteView);
        // On cree la liste de synonimes. On ecoute le click et la suppression de synonimes
        RecyclerView recyclerViewSynonime = findViewById(R.id.recyclerViewSynonimeNoteView);
        adapterSynonime = new AdapterMiniPersistableListDeletable(synonimeListener);
        recyclerViewSynonime.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSynonime.setAdapter(adapterSynonime);
        // On cree la liste de traductions. On ecoute le click et la suppression de traductions
        RecyclerView recyclerViewTraduction = findViewById(R.id.recyclerViewTradcutionNoteView);
        adapterTraduction = new AdapterMiniPersistableListDeletable(traductionListener);
        recyclerViewTraduction.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTraduction.setAdapter(adapterTraduction);
        // On instancie le viewModel qui contiendra la note
        viewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        // On fixe les listener de chaque bouton
        setEditButtonsListeners();
        setAddButtonsListener();
        Log.i(TAG, "onCreate: End");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: Start");
        // On recupere l'id de la note, s'il n'y en a pas, on ne vas pas plus loin
        Intent intent = getIntent();
        if(!intent.hasExtra(EXTRA_NOTE))
            finish();
        int noteId = Objects.requireNonNull(intent.getExtras()).getInt(EXTRA_NOTE);
        // si on a bien l'id, on recupere la note depuis la base de donnees
        notePersister = new NotePersister(this);
        // On donne la note au viewModel et
        // on ecoute ses modification.
        // Cette ecoute est necessaire pour mettre a jour les listes de synonimes et de traductions a chaque changement
        viewModel.setNote(notePersister.getNote(noteId));
        viewModel.listen(this, viewModelListner);
        // On instantie aussi le persister de categories
        categoryPersister = new CategoryPersister(this);
        // On affiche la note qui est contenue dans le viewModel
        displayNote();
        displayTraductions();
        displaySynonimes();
        Log.i(TAG, "onStart: End");
    }

    private void displayNote() {
        Log.i(TAG, "displayNote: Start");
        // Pour afficher la note,
        // On la recupere du view model, s'il n'y en a pas, on s'errete la (petit truc de parano)
        Note note = viewModel.getNote();
        if(note == null)
            return;
        // On affiche les textes de la notes
        txtvExpression.setText(note.getExpression());
        txtvComment.setText(note.getCommentaire());
        txtvCategory.setText(note.getCategory().getTitre());
        Log.i(TAG, "displayNote: End");
    }
    private void displayTraductions() {
        Log.i(TAG, "displayTraductions: Start");
        Note note = viewModel.getNote();
        if(note == null)
            return;
        adapterTraduction.setData(new ArrayList<Persistable>(note.getTraductions()));
        Log.i(TAG, "displayTraductions: End");
    }
    private void displaySynonimes() {
        Log.i(TAG, "displaySynonimes: Start");
        Note note = viewModel.getNote();
        if(note == null)
            return;
        adapterSynonime.setData(new ArrayList<Persistable>(note.getSynonimes()));
        Log.i(TAG, "displaySynonimes: End");
    }
    private void setEditButtonsListeners() {
        Log.i(TAG, "setEditButtonsListeners: Start");
        this.btnEditExpression.setOnClickListener(onEditExpression);
        this.btnEditComment.setOnClickListener(onEditComment);
        this.btnEditCategory.setOnClickListener(onEditCategory);
        Log.i(TAG, "setEditButtonsListeners: End");
    }
    private void setAddButtonsListener() {
        Log.i(TAG, "setAddButtonsListener: Start");
        this.btnAddSynonime.setOnClickListener(onAddSynonime);
        this.btnAddTraduction.setOnClickListener(onAddTraduction);
        Log.i(TAG, "setAddButtonsListener: End");
    }

    private View.OnClickListener onAddTraduction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "onAddTraduction onClick: Start");
            DialogEditText dialog = new DialogEditText(getString(R.string.str_traduction));
            dialog.setListener(new DialogEditText.Listener() {
                @Override
                public void onOk(String text) {
                    Log.i(TAG, "onAddTraduction onOk: Start");
                    Traduction traduction = new Traduction(text, viewModel.getNote());
                    viewModel.addTraductions(traduction);
                    notePersister.update(viewModel.getNote());
                    Log.i(TAG, "onAddTraduction onOk: End");
                }
            });
            dialog.show(getSupportFragmentManager(), "add-traduction");
            Log.i(TAG, "onAddTraduction onClick: End");
        }
    };
    private View.OnClickListener onAddSynonime = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "onAddSynonime onClick: Start");
            // L'utilisaateur veut ajouter un synonime.
            // On propose a l'utilisateur d'editer une toute nouvelle note comme synonime
            // Ou d'en choisir une deja presente
            CharSequence[] choix = new CharSequence[]{
                    getString(R.string.btn_autre_synonime_note),
                    getString(R.string.btn_new_synonime_note)
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(NoteViewActivity.this);
            builder.setItems(choix, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(which == 0) {
                        // Si l'utilisateur veut selectionner une autre note comme synonime
                        chooseSynonime();
                    }
                    else {
                        editSynonime();
                    }
                }
            });
            builder.create().show();
            Log.i(TAG, "onAddSynonime onClick: End");
        }
    };
    private void editSynonime() {

        // Edition d'un synonime en deux temps : expression => categorie
        DialogEditText dialog = new DialogEditText(getString(R.string.hint_synonime));
        dialog.setListener(new DialogEditText.Listener() {
            @Override
            public void onOk(final String text) {
                // On a editer l'expression
                NoteDialogChooseCategorie dialog = NoteDialogChooseCategorie.newInstance(categoryPersister.getAll());
                dialog.setOnChooseCategoryListener(new NoteDialogChooseCategorie.OnChooseCategoryListener() {
                    @Override
                    public void onChoose(Category category) {
                        // Si l'utilisateur a choisi une categorie
                        Note synonime = new Note(text, "", category);
                        viewModel.addSynonimes(synonime);
                        notePersister.update(viewModel.getNote());
                    }
                    @Override
                    public void onCancel() {
                        // si l'utilisateur n'a pas choisi de categorie, on donne la categorie globale
                        Note synonime = new Note(text, "", categoryPersister.getGlobal());
                        viewModel.addSynonimes(synonime);
                        notePersister.update(viewModel.getNote());}
                });
                dialog.show(getSupportFragmentManager(), "edit-category-synonime-note-view");
            }
        });
        dialog.show(getSupportFragmentManager(), "edit-category-synonime-note-view");
    }
    private void chooseSynonime() {
        NoteDialogChooseSynonime dialog = NoteDialogChooseSynonime.newInstance(notePersister.getAll());
        dialog.setOnChooseSynonimeListener(new NoteDialogChooseSynonime.OnChooseSynonimeListener() {
            @Override
            public void onChoose(Note synonime) {
                if(viewModel.isAlreadyIn(synonime)) {
                    Toast.makeText(NoteViewActivity.this, R.string.msg_synonime_alreadyin, Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.i(TAG, "onAddSynonime onChoose: Start");
                // L'utilisateur a choisi un synonime a ajouter
                // On l'ajoute au view model et on met a jour la base de donnees
                viewModel.addSynonimes(synonime);
                notePersister.update(viewModel.getNote());
                Log.i(TAG, "onAddSynonime onChoose: End");
            }
            @Override
            public void onCancel() {}
        });
        dialog.show(getSupportFragmentManager(), "choose-synonime");
    }
    private View.OnClickListener onEditCategory = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "onEditCategory onClick: Start");
            // L'utilisateur veut modifier la categorie de la note
            // On ouvrir le selectionneur de categorie
            NoteDialogChooseCategorie dialog = NoteDialogChooseCategorie.newInstance(categoryPersister.getAll());
            dialog.setOnChooseCategoryListener(new NoteDialogChooseCategorie.OnChooseCategoryListener() {
                @Override
                public void onChoose(Category category) {
                    // On recoit le choix de l'utilisateur
                    // On met a jour la base de donnees et le viewModel
                    viewModel.setNoteCategory(category);
                    notePersister.update(viewModel.getNote());
                }
                @Override
                public void onCancel() {}
            });
            dialog.show(getSupportFragmentManager(), "choose-category-note-view");
            Log.i(TAG, "onEditCategory onClick: Start");
        }
    };
    private View.OnClickListener onEditComment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut modifier le commentaire de la note
            DialogEditText dialog = new DialogEditText(getString(R.string.str_comment));
            dialog.setListener(new DialogEditText.Listener() {
                @Override
                public void onOk(String text) {
                    // On recoit le texte de l'utilisateur.
                    // On ajoute le commentaire au viewModel et on modifie la note dans la base de donnees
                    viewModel.setNoteCommentaire(text);
                    notePersister.update(viewModel.getNote());
                }
            });
            dialog.show(getSupportFragmentManager(), "edit-comment");
        }
    };
    private View.OnClickListener onEditExpression = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // L'utilisateur veut modifier l'expression de la note
            DialogEditText dialog = new DialogEditText(getString(R.string.str_expression));
            dialog.setListener(new DialogEditText.Listener() {
                @Override
                public void onOk(String text) {
                    // On recoit le texte de l'utilisateur.
                    // On ajoute l'expression au viewModel et on modifie la note dans la base de donnees
                    viewModel.setNoteExpression(text);
                    notePersister.update(viewModel.getNote());
                }
            });
            dialog.show(getSupportFragmentManager(), "edit-expression");
        }
    };

    private AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener synonimeListener = new AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener() {
        @Override
        public void onClick(Persistable p) {
            // L'utilisateur clique sur un synonime. On ne fait rien
        }
        @Override
        public void onDelete(Persistable p) {
            // L'utilisateur veut supprimer un synonime de la note
            // On supprime le synonime de la base de donnees
            DataBase dataBase = new DataBase(NoteViewActivity.this);
            dataBase.removeSynonimeFrom(
                    viewModel.getNote(),
                    (Note)p
            );
            // Puis on le supprime du viewModel
            viewModel.deleteSynonime((Note)p);
        }
    };
    private AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener traductionListener = new AdapterMiniPersistableListDeletable.MiniPersistableListDeletableListener() {
        @Override
        public void onClick(Persistable p) {
            // L'utilisateur veut modifier une traduction
            DialogEditText dialog = new DialogEditText(getString(R.string.str_traduction));
            dialog.setListener(new DialogEditText.Listener() {
                @Override
                public void onOk(String text) {
                    Traduction traduction = new Traduction(text, viewModel.getNote());
                    viewModel.addTraductions(traduction);
                    notePersister.update(viewModel.getNote());
                }
            });
            dialog.show(getSupportFragmentManager(), "add-traduction");
        }
        @Override
        public void onDelete(final Persistable p) {
            // L'utilisateur veut supprimer une traduction
            // On laisse une chance a l'utilisateur de ne pas supprimer la traduction
            SnackBarPendingOperation.snack(
                    findViewById(R.id.layoutNoteViewActivity),
                    getString(R.string.msg_pedding_traduction_delete),
                    new SnackBarPendingOperation.OnDismissListener() {
                        @Override
                        public void onAccepted() {
                            // Si l'utilisateur a laisser la suppression se poursuivre,
                            // On supprime la traduction de la base de donnees
                            DataBase dataBase = new DataBase(NoteViewActivity.this);
                            dataBase.delete(p);
                            // Et on la supprime du viewModel
                            viewModel.deleteTraduction((Traduction) p);
                        }
                    });
        }
    };

    private NoteViewModel.ChangeListener viewModelListner = new NoteViewModel.ChangeListener() {
        @Override
        public void onNoteChange(Note note) {
            Log.i(TAG, "ViewModelListener onNoteChange: ");
            displayNote();
        }

        @Override
        public void onSynonimesChange(List<Note> synonimes) {
            displaySynonimes();
        }

        @Override
        public void onTraductionsChange(List<Traduction> traductions) {
            displayTraductions();
        }

        @Override
        public void onClose() {}
    };
}
